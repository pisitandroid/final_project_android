package com.example.inventory

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.inventory.adapter.getFormattedAmount
import com.example.inventory.adapter.getFormattedPrice
import com.example.inventory.data.Item
import com.example.inventory.databinding.FragmentItemDetailBinding
import com.example.inventory.databinding.FragmentShopListBinding

class ShopListFragment : Fragment() {

    private val navigationArgs: ShopListFragmentArgs by navArgs()

    lateinit var item: Item
    private var _binding: FragmentShopListBinding? = null
    private val binding get() = _binding!!

    private val viewModel: InventoryViewModel by activityViewModels {
        InventoryViewModelFactory(
            (activity?.application as InventoryApplication).database.itemDao()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentShopListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        viewModel.retrieveItem(id).observe(this.viewLifecycleOwner) { selectedItem ->
            item = selectedItem
            bind(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun bind(item: Item) {
        binding.apply {
            itemName.text = item.itemName
            itemAmount.text = item.getFormattedAmount()
            binding.buttonplus.setOnClickListener { viewModel.plusAmount(item) }
            binding.buttonminus.setOnClickListener { viewModel.minusAmount(item) }
            binding.saveAction.setOnClickListener {
                val action = ShopListFragmentDirections.actionShopListFragmentToShopFragment(it.id)
                findNavController().navigate(action)
            }
        }
    }
}